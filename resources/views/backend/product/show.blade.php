@extends('backend.app')

@section('content')
<div class="container mt-5">
    <br>
    <h3>Product List</h3>
    <a href="{{ route('product.index') }}" class="btn btn-success mb-2">Back To Product List</a>

    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">title</th>
                <th scope="col">Category</th>
                <th scope="col">Description</th>
                <th scope="col">Prize</th>
                <th scope="col">
                    @if(count($products->comments) > 0)
                    <p class="">Comments <span class="badge badge-primary">{{count($products->comments)}}</span></p>
                    
                    @else
                    <p>Comment</p>
                    @endif
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{$products->title}}</td>
                <td>{{ $products->category->title }}</td>
                <td>{{ $products->description}}</td>
                <td>{{ $products->prize}}</td>
                <td>
                    <ul>
                        @foreach($products->comments as $comment)
                        <li>
                            {{$comment->body}}
                            <small>
                                {{$comment->created_at->diffForHumans()}}
                                <mark>{{$comment->created_at->toFormattedDateString()}}</mark>
                            </small>
                        </li>
                        @endforeach
                    </ul>


                </td>
            </tr>

        </tbody>
    </table>
    {{-- comment box --}}
    <form action="{{route('product/comment',$products->id)}}" method="post" style="width:
        100%">
        @csrf
        <div class="form-group">
            <label for="comment">Comment</label>
            <textarea rows="3" class="form-control" id="comment" name="comment"></textarea>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
</div>
@endsection