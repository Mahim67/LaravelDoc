@extends('welcome')

@section('content')

<div class="container">
    <h4>Edit User</h4><br><br>

    <a href="{{ route('user.index') }}" class="btn btn-primary">Back</a>
    {!! Form::model($user,[
    'route' => ['user.update', $user->id],
    'method' => 'PUT'])
    !!}

    <div class="form-group row">
        {!! Form::label('name','Name :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('name',null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('email','Email :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('email',null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('bio','Bio :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::textarea('bio',$user->profile->bio, ['class'=>'form-control','rows'=>'3']) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('facebook_url','Facebook :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('facebook_url',$user->profile->facebook_url, ['class'=>'form-control']) !!}
        </div>
    </div>
    
    <div class="form-group">
        {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}



</div>

@endsection