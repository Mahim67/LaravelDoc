@extends('welcome')

@section('content')
<div class="container">
    <h3>All User Here</h3><br><br>
    <a href="{{ route('user.index') }}" class="btn btn-outline-primary">Back</a>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Body</th>
                <th scope="col">Bio</th>
                <th scope="col">Facebook</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{$user->profile->bio}}</td>
                <td>{{$user->profile->facebook_url}}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection