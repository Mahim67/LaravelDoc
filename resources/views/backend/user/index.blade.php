@extends('welcome')

@section('content')
<div class="container">
    <h3>All User Here</h3><br><br>
    {{-- <a href="{{ route('user.create') }}" class="btn btn-success">Create Post</a> --}}
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Body</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($user as $singleUser)
            <tr>
                <th scope="row">1</th>
                <td>{{ $singleUser->name }}</td>
                <td>{{ $singleUser->email }}</td>
                <td>{{$singleUser->profile->bio}}</td>
                <td>
                    <a href="{{ route('user.show', $singleUser->id) }}" class="btn btn-primary">Show</a>
                    <a href="{{ route('user.edit', $singleUser->id) }}" class="btn btn-warning">Edit</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection