@extends('welcome')

@section('content')
<div class="container">
    <h3>All Post Here</h3><br><br>
    <a href="{{ route('post.create') }}" class="btn btn-success">Create Post</a>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">title</th>
                <th scope="col">Body</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($allData as $data)
            <tr>
                <th scope="row">1</th>
                <td>{{$data->title}}</td>
                <td>{{$data->body}}</td>
                <td>
                    <a href="" class="btn btn-primary">Show</a>
                    <a href="" class="btn btn-warning">Edit</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection