@extends('welcome')

@section('content')

<div class="container">
    <h4>Create Post</h4><br><br>

    <a href="{{ route('post.index') }}" class="btn btn-primary">Back</a>
    {!! Form::open
    (['route' => 'post.store',
    'method' => 'POST'])
    !!}

    <div class="form-group row">
        {!! Form::label('title','Title :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('title','', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group row">
        {!! Form::label('body','Body :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::textarea('body','', ['class'=>'form-control' ,'rows'=>'3']) !!}
        </div>
    </div>
    {{-- <div class="form-group row">
        {!! Form::label('image','Picture :' ,['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::file('image', $images, ['class' => 'form-control']); !!}
        </div>
    </div> --}}
    <div class="form-group">
        {!! Form::submit('submit', ['class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}



</div>

@endsection