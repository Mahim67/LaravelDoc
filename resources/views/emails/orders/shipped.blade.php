@component('mail::message')
# Introduction

The Order has been shipped!

@component('mail::button', ['url' => '','class' => 'btn btn-success'])
View Order
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
