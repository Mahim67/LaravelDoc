<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;
use App\Jobs\SendOrderEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function index(){
        $order = Order::findOrFail( rand(1,50) );
        SendOrderEmail::dispatch($order);
        
        Log::info('Dispatched Order' . $order->id );

        return 'Dispatched Order' . $order->id;

    }
}
