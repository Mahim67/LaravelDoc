<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Jobs\SendTestMail;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function processQueue()
    {
        $emailJob = new SendTestMail();
        // $emailJob = new SendTestMail()->delay(Carbon::now()->addMinutes(5));
        // $emailJob;
        SendTestMail::dispatch($emailJob)->delay(Carbon::now()->addSeconds(5));
        // dispatch($emailJob);
    }
}
