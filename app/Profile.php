<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $guarded = [];
    // protected $fillable = ['bio'];


        public function user()
        {
            return $this->belongsTo(User::class);
        }

}
