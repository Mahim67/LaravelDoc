<?php

namespace App;

use App\Comment;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{

    protected $fillable = ['title','url'];

    public function comments(){

        return $this->morphMany('App\Comment','commentable');

    }
}
