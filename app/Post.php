<?php

namespace App;

use App\Image;
use App\Comment;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // protected $fillable = ['title', 'body'];
    protected $guarded = [];

    public function comments(){

        return $this->morphMany('App\Comment','commentable');

    }

    public function image(){

        return $this->morphOne('App\Image','imageable');

    }

}
