<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('backend/app');
});

Route::resource('post', 'PostController');
Route::resource('user', 'UserController');
Route::get('send-email', 'UserController@sendMail');
Route::get('test-email', 'JobController@processQueue');
Route::get('test', 'MailController@index');
Route::resource('category', 'CategoryController');
Route::resource('product', 'ProductController');
Route::post('product/{product}/comment', 'CommentController@store')->name('product/comment');

